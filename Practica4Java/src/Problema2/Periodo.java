//Code generated by Papyrus Java
//--------------------------------------------------------

package Problema2;

/************************************************************/
/**
* 
*/
public class Periodo {
	/**
	 * @param tiempoCosecha
	 * @param cantCosechaXtimpo
	 */
	public Periodo(String tiempoCosecha, float cantCosechaXtimpo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaXtimpo = cantCosechaXtimpo;
	}

	/**
	 * 
	 */
	public Periodo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private String tiempoCosecha;
	/**
	 * 
	 */
	private float cantCosechaXtimpo;

	/**
	 * 
	 * @return 
	 */
	public float costoCosecha() {
		return cantCosechaXtimpo;
	}

	/**
	 */
	public void gananciaEstimada() {
	}

	public String getTiempoCosecha() {
		return tiempoCosecha;
	}

	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}

	public float getCantCosechaXtimpo() {
		return cantCosechaXtimpo;
	}

	public void setCantCosechaXtimpo(float cantCosechaXtimpo) {
		this.cantCosechaXtimpo = cantCosechaXtimpo;
	}

	@Override
	public String toString() {
		return "Periodo [tiempoCosecha=" + tiempoCosecha + ", cantCosechaXtimpo=" + cantCosechaXtimpo + "]";
	}
}


