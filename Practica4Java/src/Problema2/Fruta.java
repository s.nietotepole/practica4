//--------------------------------------------------------
//Code generated by Papyrus Java
//--------------------------------------------------------

package Problema2;

/************************************************************/
/**
* 
*/
public class Fruta {
	/**
	 * @param nombre
	 * @param extesion
	 * @param costoPromedio
	 * @param precioVentaProm
	 * @param periodos
	 */
	public Fruta(String nombre, float extesion, float costoPromedio, float precioVentaProm, Periodo periodos) {
		super();
		this.nombre = nombre;
		this.extesion = extesion;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
		this.periodos = periodos;
	}

	/**
	 * 
	 */
	public Fruta() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private String nombre;
	/**
	 * 
	 */
	private float extesion;
	/**
	 * 
	 */
	private float costoPromedio;
	/**
	 * 
	 */
	private float precioVentaProm;
	/**
	 * 
	 */
	private Periodo periodos;

	/**
	 * 
	 * @param p 
	 */
	public void agregarPeriodo(Periodo p) {
	}

	/**
	 * 
	 * @param i 
	 * @return 
	 */
	public boolean eliminarPeriodo(int i) {
		return false;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getExtesion() {
		return extesion;
	}

	public void setExtesion(float extesion) {
		this.extesion = extesion;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public Periodo getPeriodos() {
		return periodos;
	}

	public void setPeriodos(Periodo periodos) {
		this.periodos = periodos;
	}

	@Override
	public String toString() {
		return "Fruta [nombre=" + nombre + ", extesion=" + extesion + ", costoPromedio=" + costoPromedio
				+ ", precioVentaProm=" + precioVentaProm + ", periodos=" + periodos + "]";
	}
	
}


